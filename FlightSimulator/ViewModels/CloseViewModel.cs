﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FlightSimulator.Model;

namespace FlightSimulator.ViewModels
{
    class CloseViewModel
    {
        public void MainWindow_Closed()
        {
            if (Info.Instance.TcpClient != null)
            {
                Info.Instance.TcpClient.Close();
            }
            if (Commands.Instance._client != null)
            {
                Commands.Instance._client.Close();
            }
        }
    }
}
