﻿using FlightSimulator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace FlightSimulator.ViewModels
{
    class AutoPilotViewModel :BaseNotify
    {
        public string dataAutoPilot;
        //private int counterTimes;
        //public Commands commands;
        public AutoPilotViewModel()
        {
            //commands = 
            dataAutoPilot = "";
        }

        public string DataAutoPilot
        {
            get
            {
                NotifyPropertyChanged("ColorChanged");
                return dataAutoPilot;
            }
            set
            {
                dataAutoPilot = value;
                //NotifyPropertyChanged("dataAutoPilot");
               // NotifyPropertyChanged("ColorChanged");
            }
        }
        public String color;
        public String ColorChanged
        {
            get
            {
                if(dataAutoPilot == "")
                {
                    color = "White";
                }
                else
                {
                    color = "Pink";
                }
                return color;
            }
        }

        private ICommand _clearCmd;
        public ICommand clearCommand
        {
            get
            {
                return _clearCmd ?? (_clearCmd =
                new CommandHandler(() => clearWindow()));
            }
        }
        private void clearWindow()
        {
            dataAutoPilot = "";
            NotifyPropertyChanged(dataAutoPilot);
            
        }


        private ICommand _sendData;
        public ICommand sendData
        {
            get
            {
                return _sendData ?? (_sendData =
                    new CommandHandler(() => SendButton()));
            }
        }
        private void SendButton()
        {
            Commands.Instance.sendData(dataAutoPilot);
            dataAutoPilot = "";
            NotifyPropertyChanged(dataAutoPilot);
        }
    }
}
