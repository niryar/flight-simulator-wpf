﻿using System.Net.Sockets;
using System.Net;
using System;
using System.Threading;

using FlightSimulator.Model;
using System.Text;
using FlightSimulator.ViewModels;

namespace FlightSimulator.Model
{
    class Info
    {
        private static Info _Instance = null;
        public TcpClient TcpClient;
        FlightBoardViewModel flightBoardViewModel;

        public FlightBoardViewModel FlightBoardViewModel
        {
            get { return flightBoardViewModel; }
            set { flightBoardViewModel = value; }
        }

        public static Info Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Info();
                }
                return _Instance;
            }
        }

        private Info() { }

        public void OpenServer()
        {
            IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse(ApplicationSettingsModel.Instance.FlightServerIP),
                ApplicationSettingsModel.Instance.FlightInfoPort);
            TcpListener server = new TcpListener(iPEndPoint);
            server.Start();

            Console.WriteLine("Waiting for a connection...");

            TcpClient = server.AcceptTcpClient();

            Console.WriteLine("A client connected.");

            NetworkStream stream = TcpClient.GetStream();

            Byte[] bytes;

            Thread thread = new Thread(() =>
            {
                while (true)
                {
                    if (TcpClient.Connected && TcpClient.ReceiveBufferSize > 0)
                    {
                        bytes = new byte[TcpClient.ReceiveBufferSize];
                        //if (stream.CanRead)
                        //{
                            stream.Read(bytes, 0, TcpClient.ReceiveBufferSize);
                            string data = Encoding.ASCII.GetString(bytes);
                            splitData(data);
                        //}

                    }

                }
                TcpClient.Close();

            });
            thread.Start();
        }
        public void splitData(string data)
        {
            string[] splitedData = data.Split(',');
            if(data.Contains(","))
            {
            FlightBoardViewModel.Lon = double.Parse(splitedData[0]);
            FlightBoardViewModel.Lat = double.Parse(splitedData[1]);

            Console.WriteLine(FlightBoardViewModel.Lon);
            Console.WriteLine(FlightBoardViewModel.Lat);
            }


        }
    }
}