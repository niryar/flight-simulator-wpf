﻿using FlightSimulator.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FlightSimulator.Model;

namespace FlightSimulator.ViewModels
{
    class FlightBoardButtons
    {
        private ICommand _connectCommand;
        private ICommand _settingsCommand;
        private SettingsWindow settingsWindow;
        //Boolean IsDisconnected;

        public ICommand ConnectCmd
        {
            get
            {
                return _connectCommand ?? (_connectCommand =
                new Model.CommandHandler(() => Connect()));
            }
        }

        public ICommand OpenSettings
        {
            get
            {
                return _settingsCommand ?? (_settingsCommand =
                new Model.CommandHandler(() => OpenSettingsWindow()));
            }
        }


        private void Connect()
        {
            Info.Instance.OpenServer();
            Commands.Instance.ConnectClient();
            //IsDisconnected = false; // Setting that the server is connected
            //Console.WriteLine("Ariel the king");
        }

        private void OpenSettingsWindow()
        {
            settingsWindow = new SettingsWindow();
            settingsWindow.Show();
        }
    }
}
