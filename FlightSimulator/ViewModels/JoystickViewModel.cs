﻿using FlightSimulator.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FlightSimulator.Model;

class JoystickViewModel
{
    private string elevatorPath = "set controls/flight/elevator";
    private string rudderPath = "set controls/flight/rudder";
    private string throttlePath = "set controls/engines/current-engine/throttle";
    private string aileronPath = "set controls/flight/aileron";

    public double RudderChanged
    {
        set
        {
            string CommandData = rudderPath + " " + value + "\r\n";
            Commands.Instance.sendData(CommandData);
        }
    }

    public double ThrottleChanged
    {
        set
        {
            string CommandData = throttlePath + " " + value + "\r\n";
            Commands.Instance.sendData(CommandData);
        }
    }

    public double ElevatorChanged
    {
        set
        {
            string CommandData = elevatorPath + " " + value + "\r\n";
            Commands.Instance.sendData(CommandData);
        }
    }

    public double AileronChanged
    {
        set
        {
            string CommandData = aileronPath + " " + value + "\r\n";
            Commands.Instance.sendData(CommandData);
        }
    }
}
