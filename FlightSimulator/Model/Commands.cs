﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using FlightSimulator.Model.Interface;

namespace FlightSimulator.Model
{
    class Commands
    {
        private static Commands _Instance = null;
        public TcpClient _client;
        bool _isConnected;


        public static Commands Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Commands();
                }
                return _Instance;
            }
        }


        private Commands()
        {
            _isConnected = false;
        }

        public void ConnectClient()
        {
            IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse(ApplicationSettingsModel.Instance.FlightServerIP),
            ApplicationSettingsModel.Instance.FlightCommandPort);
            _client = new TcpClient();
            _client.Connect(iPEndPoint);
            Console.WriteLine("You are connected");
            _isConnected = true;

        }

        public void sendData(string data)
        {
            string[] chunks = parseText(data);
          
            Thread thread = new Thread(() =>
            {
                if (!_isConnected) return;
                NetworkStream ns = _client.GetStream();

                foreach (string chunk in chunks)
                {
                    // Send data to server
                    string command = chunk;
                    command += "\r\n";
                    byte[] buffWriter = Encoding.ASCII.GetBytes(command);
                    if (ns.CanWrite)
                    {
                        ns.Write(buffWriter, 0, buffWriter.Length);
                    }
                    System.Threading.Thread.Sleep(2000);
                }
            });
            thread.Start();
            
        }


        public string[] parseText(string data)
        {
            string[] chunks;
            chunks = data.Split('\n');
            return chunks;
        }
    }
}

/**
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;


using System.Net;




namespace FlightSimulator
{
    public class Commands
    {

        public Dictionary<string, double> pathRead = new Dictionary<string, double>();
        public Dictionary<string, string> dict = new Dictionary<string, string>();

        NetworkStream ns;
        public void connectServer()
        {

            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5402);
            TcpClient client = new TcpClient();
            client.Connect(ep);
            Console.WriteLine("You are connected");
            NetworkStream ns = client.GetStream();

        }
        public void setData()
        {

            this.pathRead.Add("set /instrumentation/airspeed-indicator/indicated-speed-kt", 0);
            this.pathRead.Add("set /instrumentation/altimeter/indicated-altitude-ft", 0);
            this.pathRead.Add("set /instrumentation/altimeter/pressure-alt-ft", 0);
            this.pathRead.Add("set /instrumentation/attitude-indicator/indicated-pitch-deg", 0);
            this.pathRead.Add("set /instrumentation/attitude-indicator/indicated-roll-deg", 0);
            this.pathRead.Add("set /instrumentation/attitude-indicator/internal-pitch-deg", 0);
            this.pathRead.Add("set /instrumentation/attitude-indicator/internal-roll-deg", 0);
            this.pathRead.Add("set /instrumentation/encoder/indicated-altitude-ft", 0);
            this.pathRead.Add("set /instrumentation/encoder/pressure-alt-ft", 0);
            this.pathRead.Add("set /instrumentation/gps/indicated-altitude-ft", 0);
            this.pathRead.Add("set /instrumentation/gps/indicated-ground-speed-kt", 0);
            this.pathRead.Add("set /instrumentation/gps/indicated-vertical-speed", 0);
            this.pathRead.Add("set /instrumentation/heading-indicator/indicated-heading-deg", 0);
            this.pathRead.Add("set /instrumentation/magnetic-compass/indicated-heading-deg", 0);
            this.pathRead.Add("set /instrumentation/slip-skid-ball/indicated-slip-skid", 0);
            this.pathRead.Add("set /instrumentation/turn-indicator/indicated-turn-rate", 0);
            this.pathRead.Add("set /instrumentation/vertical-speed-indicator/indicated-speed-fpm", 0);
            this.pathRead.Add("set /controls/flight/aileron", 0);
            this.pathRead.Add("set /controls/flight/elevator", 0);
            this.pathRead.Add("set /controls/flight/rudder", 0);
            this.pathRead.Add("set /controls/flight/flaps", 0);
            this.pathRead.Add("set /controls/engines/current-engine/throttle", 0);
            this.pathRead.Add("set /engines/engine/rpm", 0);
        }

        public void setDict(List<double> vector1)
        {
            this.pathRead["/instrumentation/airspeed-indicator/indicated-speed-kt"] = vector1[0];
            this.pathRead["/instrumentation/altimeter/indicated-altitude-ft"] = vector1[1];
            this.pathRead["/instrumentation/altimeter/pressure-alt-ft"] = vector1[2];
            this.pathRead["/instrumentation/attitude-indicator/indicated-pitch-deg"] = vector1[3];
            this.pathRead["/instrumentation/attitude-indicator/indicated-roll-deg"] = vector1[4];
            this.pathRead["/instrumentation/attitude-indicator/internal-pitch-deg"] = vector1[5];
            this.pathRead["/instrumentation/attitude-indicator/internal-roll-deg"] = vector1[6];
            this.pathRead["/instrumentation/encoder/indicated-altitude-ft"] = vector1[7];
            this.pathRead["/instrumentation/encoder/pressure-alt-ft"] = vector1[8];
            this.pathRead["/instrumentation/gps/indicated-altitude-ft"] = vector1[9];
            this.pathRead["/instrumentation/gps/indicated-ground-speed-kt"] = vector1[10];
            this.pathRead["/instrumentation/gps/indicated-vertical-speed"] = vector1[11];
            this.pathRead["/instrumentation/heading-indicator/indicated-heading-deg"] = vector1[12];
            this.pathRead["/instrumentation/magnetic-compass/indicated-heading-deg"] = vector1[13];
            this.pathRead["/instrumentation/slip-skid-ball/indicated-slip-skid"] = vector1[14];
            this.pathRead["/instrumentation/turn-indicator/indicated-turn-rate"] = vector1[15];
            this.pathRead["/instrumentation/vertical-speed-indicator/indicated-speed-fpm"] = vector1[16];
            this.pathRead["/controls/flight/aileron"] = vector1[17];
            this.pathRead["/controls/flight/elevator"] = vector1[18];
            this.pathRead["/controls/flight/rudder"] = vector1[19];
            this.pathRead["/controls/flight/flaps"] = vector1[20];
            this.pathRead["/controls/engines/current-engine/throttle"] = vector1[21];
            this.pathRead["/engines/engine/rpm"] = vector1[22];
        }



        public void setInfo(string path)
        {
            byte[] byteTime = Encoding.ASCII.GetBytes(DateTime.Now.ToString());

            this.ns.Write(byteTime, 0, byteTime.Length);

        }

    }
}
**/
