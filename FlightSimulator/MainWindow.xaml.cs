﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FlightSimulator.ViewModels;

namespace FlightSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CloseViewModel closeViewModel;
        public MainWindow()
        {
            InitializeComponent();
            closeViewModel = new CloseViewModel();
        }

        private void PlaneController_Loaded(object sender, RoutedEventArgs e)
        {
            

        }

        private void FullFlightBoard_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            closeViewModel.MainWindow_Closed();
        }

    }   
}
